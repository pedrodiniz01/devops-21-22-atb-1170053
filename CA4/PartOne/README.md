# Class Assignment 4 Report - Part 1

### **Containers with Docker**

The goal of this assignment is to use Docker to execute the chat application.

For this Class Assignment Part 1, I created the following issues:

* Issue #16 : Create Dockerfile to build chat server;
* Issue #17 : Create Dockerfile - copy the jar file;
* Issue #18 : Tag the docker image and publish it in a docker hub;
* Issue #19 : README.md file for this Class Assignment 4, Part 1.

## 1. Analysis, Design and Implementation

To start, I installed docker on my computer, using the following command:

> sudo apt install docker.io

Then, to see the version of docker I ran:

> docker -v

The version was "Docker version 20.10.12, build 20.10.12-Oubuntu4".

### 1.1 Create docker images and running containers using the chat application from CA2

#### **1.1.1. Create a docker image to execute the chat server:**

In order to execute the chat server using Docker I created a dockerfile which is a text document that contains all the commands to assemble an image.

The main goal of the first part of the assignment is to create two versions of my solution to create docker images and run containers using the chat application from CA2.

First, I'll start by referring the first version for which I created a version to build the chat server "inside" the Dockerfile.

* **1) Chat server built "inside" the Dockerfile:**

For this version I created from scratch a Dockerfile which is inside folder "CA4/PartOne/VersionOne" and it can be seen below:

![](./Images/0.png)

- First, I defined with the *FROM* command the version of the ubuntu;
- Then I used the java version 11 and added git dependency;
- Next we can see the clone command for my private repository;
- For the repository I defined my working directory to use gradle basic demo, where the chat server app source code is present;
- I added some commands related to gradle wrapper to allow permissions and to build the project;
- Lastly, I set the container communication port to 59001 and the chat server is executed.


After I build the container using the following command:

> docker build . -t chat_docker_1

And it built succesfully:

![](./Images/1.png)

Then I had to start the container, using the following command:

> docker run -p 59001:59001 -d chat_docker_1

![](./Images/13.png)


Then in the host machine I used the command to start the application:

> ./gradlew runClient 

![](./Images/14.png)
 

* **2) Chat server built in the Host Computer and copy the jar file "into" the Dockerfile:**

The second version had the goal of building the chat server in the host computer and copy the generated jar file into the docker file.

To start, I created a new folder inside the "CA4/PartOne/VersionTwo" folder and copied the executable .jar file that was generated.

![](./Images/11.png)

For this new Dockerfile I had to add few dependencies:
![](./Images/12.png)

- So I started to define the *openjdk_11*, pulled from DockerHub.
- Then I created a new directory named "*/root*" which will be the WORKDIR.
- And defined the COPY of this jar file.
- As before, it is defined the communication port as 59001.
- Lastly, I presented the chat Server command to be executed when the container is started.

Then I ran the commands to build and run the new container:

> docker build . -t chat_docker_2

![](./Images/15.png)

> 
> docker run -p 59001:59001 -d chat_docker_2

![](./Images/16.png)


After this, just like before, in the host I ran the command 
to start the chat application:

> ./gradlew runClient

![](./Images/17.png)


### 1.2 Tag the image and publish it in docker hub

First I created an account in DockerHub in order to publish the images created and save them.
To save them I also had to create a new repository.

First, I started to search the images I had, and verify if they really existed using the command:

> docker image ls

![](./Images/18.png)

Then I logged in via terminal to DockerHub:

![](./Images/7.png)

Then I tagged the image marking the repository I had created:

![](./Images/19.png)

Next I pushed the images to the remote repository:

![](./Images/20.png)

And checked on dockerhub:

![](./Images/21.png)


To end this first part of the Class Assignment 4 I added the changes,
committed them closing the issue #16, #17, #18, #19 and marked the repository
with the tag **"ca4-part1"**. 








