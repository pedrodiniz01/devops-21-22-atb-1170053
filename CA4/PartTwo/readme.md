# Class Assignment 4 

## *Containers With Docker*

The goal of this assignment is to use Docker containers to run the basic tutorial application (web+db).

For this Class Assignment Part 2, I created the following issues:

* Issue #16 : Change docker compose file;
* Issue #18 : Investigate about alternative: Kubernetes;
* Issue #17 : Write readme file.

### 1. Docker Compose Setup

I started by clonning the teacher's project from: https://bitbucket.org/atb/docker-compose-spring-tut-demo/src/master/.

Then I change the docker file of the web folder to use my own repository: 

    RUN git clone https://pedrodiniz01@bitbucket.org/pedrodiniz01 devops-21-22-atb-1170053.git

    WORKDIR /tmp/build/devops-21-22-atb-1170053/CA3/PartTwo/gradle_react-and-spring-data-rest-basic

On the docker-compose.yml I configured the IP address:

![](./images/11.png)

After those changes, in the folder of the docker-compose.yml the following command was executed on Terminal: 

    docker-compose build

The build was succesful:    

![](./images/1.png)

Then I ran:

    docker-compose up

![](./images/2.png)

After I checked if both containers were running, using:

    docker ps -a

![](./images/5.png)

On the browser, I checked the: http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT

![](./images/3.png)

The http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console shows the database:

![](./images/4.png)

### 2. Docker Hub

First I logged on docker hub via terminal:

    docker login

Then I created both tags and pushed the images to the repository:

    sudo docker tag docker-compose-spring-tut-demo_db pedrodiniz0101/ca4_part2:docker-compose-spring-tut-demo_db

    docker push pedrodiniz0101/ca4_part2:docker-compose-spring-tut-demo_db

![](./images/6.png)

    sudo docker tag docker-compose-spring-tut-demo_web pedrodiniz0101/ca4_part2:docker-compose-spring-tut-demo_web

    sudo docker push pedrodiniz0101/ca4_part2:docker-compose-spring-tut-demo_web

![](./images/7.png)

Then I checked on docker hub if everything was ok:

![](./images/8.png)

## 3. Database Backup

The next step is to copy the database file to the host machine, so I accessed the database container:

        sudo docker exec -it 44a09d771ca8 bash


After accessing I copied the jpadb.mv.db file to the data folder in my machine using the following command:

    cp ./jpadb.mv.db /usr/src/data

The file was copied with success: 

![](./images/9.png)


Kubernetes
==========

The alternative chosen was Kubernetes. Kubernetes is a open source platform that manages and automates various operations with containers. 
Docker allows us to build and run containers, but when more complex tasks are needed, like managing microservices, synchronize in a multi-cloud environment, kubernetes offer more solutions to orchestrate these tasks. 

Main features of kubernetes:
- Deploy a containerized application on a cluster.
- Scale the deployment.
- Update the containerized application with a new software version.
- Debug the containerized application.


## 1. Kubernetes architecture and components


The following image shows Kubernetes functionality and its main components: 

![](./images/12.png)


The components all talk to each other through the API server:
- **Master** : The master is a collection of components which make up the control panel of Kubernetes. These components are used for all cluster decisions. It includes both scheduling and responding to cluster events.
- **Cluster** : A cluster is a set of nodes grouped together.
- **Node** : A node can be a physical or virtual machine where the containers inside the pods will be launched by Kubernetes.
- **Pod** : A pod represents a single instance of an application, and the simplest unit within the Kubernetes object model, that contain one or more containers with the same IP address and port space.

## 2. Differences between Kubernetes and Docker Swarm

![](./images/14.png)



## 3. Kubernetes: Pros and Cons

### 3.1 Pros

- Easy organization of service with pods;
- It is developed by Google, who bring years of valuable industry experience to the table;
- Largest community among container orchestration tools;
- Kubernetes can run on-premises bare metal, OpenStack, public clouds Google, Azure, AWS, etc;
- Containerization using kubernetes allows package software to serve these goals. It will enable applications that need to be released and updated without any downtime;
- Kubernetes allows you to assure those containerized applications run where and when you want and helps you to find resources and tools which you want to work.

### 3.1 Cons

- Kubenetes dashboard not as useful as it should be;
- Kubernetes is a little bit complicated and unnecessary in environments where all development is done locally;
- Security is not very effective.

## 4. Kubernetes cloud-environment

Kubernetes, and containers in general, are a natural fit for cloud environments because containers are much more portable and lightweight – and containers can run in most cloud and on-premise environments. In particular, Containers are a way to achieve a multi-cloud strategy, because they can be run across both on premise and the major cloud providers. For example, the three most used cloud based solutions (AWS, Azure, GCP) all have these solutions integrated with their services.

### 4.1 Amazon Web Service

AWS has three container environments: ECS, EKS, and Fargate.
- ECS is the best option if you have little experience with containers and already work with AWS to host your services. It’s the “container light” of the three options.
- EKS is a more complex solution, that helps with the process of building, securing, operating, and maintaining Kubernetes clusters on AWS. 
- Fargate is the new kid on the block. It’s the latest release from Amazon for container users. It’s a way for developers with no experience in underlying infrastructure to work with them. Fargate lets you deploy containers without managing servers or clusters.


### 4.2 Azure

- Microsoft offers a solution named AKS (Azure Container Service). It is reported as a slightly slower during deployments, but Microsoft has been releasing new improvements. It is available since 2015, and supports Linux images, which means that you are able to deploy linux containers. This means you aren't limited to just Windows.

### 4.3 Google Cloud Platform

- Google is the original creator for Kubernetes standards, so working with this platform puts you ahead of the game in almost every aspect.
- The main issue with GCP is that it’s not the most popular for IaaS. It doesn’t have the small business cloud offerings that AWS and Azure offer, so its platform as a whole is not attractive to corporations that want to integrate the cloud into its internal network. There is no integration of Active Directory like Azure or IAM with AWS.

### 2.4 Summary of differences between cloud solutions

![](./images/13.png)

## 5. Summary 

- Container helps an organization to perform maintenance and update without interrupting services;
- Kubernetes is an example of a container management system developed in the Google platform;
- The biggest advantage of using Kubernetes is that it can run on-premises OpenStack, public clouds Google, Azure, AWS, etc;
- Cluster, master and node are important basic of kubernetes;
- Master node and work node are important components of Kubernetes architecture;
- Docker does not allow auto-scaling while Kubernetes allows auto-scaling;
- The biggest drawback of Kubenetes is that it’s dashboard not very useful and effective,





