# Class Assignment 5 Report - Part 1

### **CI/CD Pipelines with Jenkins **

The goal of this assignment is to make a jenkins build using the 'gradle basic demo' app. I copy and pasted the app to the following directory 'CA5/PartOne/gradle_basic_demo'.

I created the following issues for this assignment:

* Issue #23 : Build in Jenkins Successful for Option 1;
* Issue #24 : Build in Jenkins Successful for Option 2;


## 1. Analysis, Design and Implementation

### 1.1 Install and Setup JENKINS

To start off I started by installing Jenkins on my computer. I downloaded the war file from [here](https://www.jenkins.io/doc/book/installing/war-file/) and installed it from the command line (since I am using ubuntu). 

>java -jar jenkins.war --httpPort=9090**

![image1](./images/1.png)


Entering the http://localhost:9090/ I had to create an administrator account with username and password and configure jenkins.

![image1](./images/12.png)

![image1](./images/13.png)

![image1](./images/2.png)

Since I took the screenshots later on, this last image already has the build made. 

After setting up jenkins, we have two different options of creating a pipeline:
- Inserting the script directly on Jenkins (Pipeline Script);
- Using Jenkins file (Pipeline Script from SCM);

### 1.2 Create Pipeline using Jenkins (Pipeline Script)

Firstly, I created a new pipeline which I named 'CA5_PartOne_v2' (this was the second pipeline I created) and inserted the following script in the Jenkins configuration:

![image1](./images/3.png)

After saving the changes I started the build of the gradle_basic_demo project.

The result of the build was successful as expected.

![image1](./images/4.png)


### 1.3 Create Pipeline using a 'Jenkinsfile' (Pipeline Script from SCM)

For this next option I created a Jenkinsfile with the same configuration as the previous build. 

The Jenkinsfile was added to the directory 'CA5/PartOne/gradle_basic_demo' and divided in different parts:

* First Stage : ***Checkout*** - where we added the git credentialsId for the ssh key and the url.
* Second Stage : ***Assemble*** - for this I used the sh command since I am using linux.
* Third Stage : ***Test*** - I added command to run gradle test (task I had already created on gradle file).
* Fourth Stage : ***Archiving*** - in this stage I only added the build/distributions path to the archiveArtefacts feature.

![image1](./images/5.png)


#### 1.3.1 Setting up ssh key

In order to link Bitbucket and Jenkins I had to create a ssh key on Bitbucket and configure it on Jenkins.

So, in the command line I wrote:

> ssh-keygen

![image1](./images/6.png)

After this, a ssh key was generated in a new created folder that contains the password:

![image1](./images/7.png)

Next, I went to BitBucket and added a new generated ssh key which I copied from the folder mentioned above *"id_rsa.pub"*.
For this key I gave the name "jenkinsKey". This name was the one added to the Jenkinsfile in the checkout stage,
in concrete with ***git credentialsId: jenkinsKey***.

![image1](./images/8.png)

Secondly I had to add the second generated key, present in the file *"id_rsa"*, to Jenkins, to this one I gave the name **pedrodiniz01(Pedro_Diniz_Credentials)**.

![image1](./images/9.png)

#### 1.3.2 Creating Jenkins Pipeline

After having the Jenkinsfile pushed into the repository (inside 'CA5/gradle_basic_demo') it was time to create a new pipeline which I named it 'CA5PartOne'.

Next, I had to configure the Pipeline, here there were some changes from the previous version.
Going into Pipeline definitions I chose *Pipeline script from SCM*, the SCM chosen was Git.

Then I had to set the Repository, for this I added the URL to my bitbucket repository, and chose the Jenkins credentials
mentioned above.

Since my bitbucket branch is set to '/main' I had to change it.

The last configuration step was the Script Path, which was the path for the created Jenkinfile in my repository,
for this, the path was the following one: "CA5/gradle_basic_demo/Jenkinsfile".

After this I only had to apply and save the changes.

![image1](./images/10.png)

The build was succesful:

![image1](./images/11.png)



To finish, since everything was working as expected I resolved the opened issues and tag my repository with the tag ***ca5-part1***.







