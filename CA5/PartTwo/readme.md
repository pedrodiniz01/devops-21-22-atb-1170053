# Class Assignment 5 - Part 2

The goal of the Part 2 of this assignment is to create a pipeline in Jenkins to build the spring boot application (developed in CA2, Part2), and, later on, implement an alternative using another continuous integration tool. The alternative chosen was Buddy CI.  

I created the following issues for this assignment:

* Issue #25 : Create pipeline for spring boot application in Jenkins;
* Issue #26 : Implement alternative in Buddy;

## 1. Jenkinsfile Configuration   

To start, I copied and pasted the spring boot application to the follow directory 'CA5/PartTwo/react-and-spring-data-rest-basic'

The Jenkinsfile and the Dockerfile are inside that directory.

Since I was having trouble running this part of the assignment on linux, I had to change to windows. The jenkins files will look very similar to the previous CA5-part1, except for two things:
- bat command will be used instead of sh;
- two more stages were added: javadoc and docker image.


### 1.1 Adding javadoc

![](./images/javadoc_changes.png)

* *javadocDir:* Refers to the directory where javadoc will be generated;
* *keepAll: true* - keeps the report of previous builds;

### 1.2 Adding docker image 

![](./images/docherhub_configuration.png)


* *docker.withRegistry:* enables the access to the Docker Hub remote repository to save the Docker image;  the second argument are the Docker credentials created similarly to the Bitbucket credentials on **Manage Credentials**.
  
* *docker.build:* will build the Docker image, taking as the first argument our remote repository, followed by the path to the Dockerfile
  starting from the workspace directory.


## 2. Dockerfile Settings

The Dockerfile has the following information:

![](./images/dockerfile.png)


Before running the Jenkins pipeline, I had to make sure that Docker Desktop was running locally.

## 3. Running Jenkins Pipeline

Having all the files ready, a new pipeline with SCM was initiated.

![](./images/creating_pipeline.png)

After many attempts, the build was successfully made!

![](./images/build_success.png)

![](./images/success.png)

To finish, a Docker image was created and uploaded to the Docker Hub. The name of Docker image had the username of Docker Hub and, the job ID of the Jenkins build, as instructed in the Jenkinsfile:

![](./images/dockerhub.png)


## 4. Analysis of an Alternative

**Jenkins vs Buddy (Theoretical)**

![](images/alternative/image01.PNG)



![](images/alternative/image02.PNG)

**Jenkins vs Buddy (Personal Experience)**

Jenkins was a good tool to learn and a challenge to set up: since the installation, setting up ssh keys, troubleshooting the failing builds. In the other hand, I felt Buddy was very use friendly, very intuitive and I implemented everything without having to install anything and barely any problem occurred.

To sum up Jenkins:

- Is a more popular solution used by various IT companies;

- It has a larger community so troubleshooting can be easier since you have more 
information available;

- It is a more powerful build but has a larger learning curve;

- It's more expandable with the amount of plug ins it has available;

In the other hand Buddy:

- Has better UI that is very intuitive;

- Requires no previous installation/setup;

- Is less popular, so if you find a problem you might have more dificulties solving it;

- Is less customizable (doesn't offer an amount of plug ins like Jenkins);

To conclude, I enjoyed both tools but felt Buddy was easier to use and more appropriate for a small project like ours. 


## 3. Implementation of the Alternative: Buddy

 To start, I register in Buddy and synchronized my bitbucket account, so I can have access to all my available repositories.
 I was having some trouble using my actual repository, because when I created a pipeline I couldn't choose the specific directory to run, in my case 'CA5/PartTwo'react-and-spring-data-rest-basic'. 
 
 To solve this issue I created a new repository in bitbucket (https://bitbucket.org/pedrodiniz01/ca5_part2_alternative/src/master/) that only has the needed application.

 This way when I create a pipeline in Buddy it runs on the right directory.

 Here I show the new repository with the application:
 
 ![](images/alternative/new_repo.png)


### 3.1 Add Gradle Action

In buddy when you are creating a pipeline you are defining a set of actions that will build the project. 

 ![](images/alternative/4.png)

To start, I choose a gradle action and set the version to 'gradle 7.4.1 jdk 11' and inserted the following commands: 

    gradle build
    gradle assemble
    gradle test
    gradle javadoc

 ![](images/alternative/gradle_build.png)

 ### 3.2 Build docker image and push it

 Then I added two new actions:
 - Build Docker image
 - Push Docker image

To build the image I choose the 'dockerfile' that was on the repository: 

 ![](images/alternative/2AddDockerImage.png)

To push the image I had to fill the form.

- Docker registry: I choose dockerhub;
- DockerHub account: I had to synchronize using my username and password.
- Repository: I created a new one in dockerhub;
- Tags: we can write anything.

 ![](images/alternative/pushimage.png)

  ### 3.3 Run the pipeline

  After a few tries the build was succesful:

 ![](images/alternative/success.png)

 I verified if the files were generated correctly:
 - Tests:

 ![](images/alternative/test.png)

 - Javadoc:

  ![](images/alternative/javadoc.png)

 - Archive:

  ![](images/alternative/archive.png)

Then I checked if the image was succesfully pushed to my dockerhub:

  ![](images/alternative/dockerhub.png)

 To finish off I committed and pushed theses changes and marked my repository with the tag ca5-part2.
