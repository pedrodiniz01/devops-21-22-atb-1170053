# Class Assignment 3 Report


## 1. Analysis, Design and Implementation

### 1.1 Second Week, Vagrant virtual environment - 09/05/2022

#### 1.1.1 Create a new backup of the application:

I started by creating a new backup folder of the react-and-spring application inside the CA3/PartTwo folder. This way I don't change the properties of the application made in the previous assignment.

![](./screenshots/1.png)

And pushed the changes:

    git add .
    git commit -m "Create new backup folder inside CA3 of react and spring app"
    git push

#### 1.1.2 Clone vagrant file

Next I cloned the Professor repository (https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/src/master/) and pushed the vagrant file to CA3/PartTwo folder.

And pushed the changes resolving a specific issue: 

    git add .
    git commit -m "Push Vagrant file resolving issue #13"
    git push

#### 1.1.3 Change vagrant file and other properties

Next I changed the vagrant file to clone my own repository:

![](./screenshots/2.png)

And followed the Professor commits to change the application (https://bitbucket.org/atb/tut-basic-gradle/commits/).

During this process I did several pushes:
- I generated a gradle .war file that was missing; 
- Changed source compability to use java 8;
- Added front end compability.

#### 1.1.4 Test the application

To boot the remote machines I run the command:

    vagrant up --provision

And the build was successful:

![](./screenshots/3.png) 

Then I check the status:

    vagrant status

![](./screenshots/4.png) 

Both machines are running.

#### 1.1.5 Check on browser

Then I checked if the front end is working:

![](./screenshots/5.png) 

Since I tested the machines several times, I created several entries.

Now it's time to check the h2 database: 

![](./screenshots/6.png)

Everything is working fine.


## 2. Analysis of an Alternative: 

As an alternative, I choose to try implement Hyper-V for a few reasons:
- It's free;
- Has good reviews;
- Has alot of documentation/support for errors;
- It's a windows tool.



### 2.1 Virtual Box vs Hyper-V (Theorethical)

- One of the main differences between both systems is that hyper-v is directly launched on the machine or physical hardware, while virtual box is an application that runs on the operating system and is installed on a host;
- Hyper-v is designed to run only on windows envirnment while virtual box runs on several different OS; 
- Hyper-V is categorized as Type-1 as it is more secure because it runs on its OS. VirtualBox is classified as Type-2 as it is hosted hypervisor and best for client use.
- Hyper-V runs at a high speed and is very smooth while doing any operations. VirtualBox has comparatively less speed because of the extra overhead software.


### 2.2 Virtual Box vs Hyper-V (Personal Experience)

Installing and setting up Hyper-v was very simple. The only issue I had was choosing the virtual box. I had to try several different boxes since I had many dependencies error. After choosing the right one, it was a very smooth experience and I didn't notice many differences between hyper-v and virtual box.

## 3. Implementation of the Alternative

Since I have windows 11 Home I had to run the following command-line script:

 	pushd "%~dp0"
 	dir /b %SystemRoot%\servicing\Packages\*Hyper-V*.mum >hv.txt
 	for /f %%i in ('findstr /i . hv.txt 2^>nul') do dism /online /norestart /add-package:"%SystemRoot%\servicing\Packages\%%i"add-package:"%SystemRoot%\servicing\Packages\%%i"
 	del hv.txt
 	Dism /online /enable-feature /featurename:Microsoft-Hyper-V -All /LimitAccess /ALL
 	pause

After the download was done, I changed the extension to a batch file and ran it as administrator. 

### 3.1 Virtual box 

After installing hyper-v I choose the box I wanted to use. I downloaded many different boxes that failed. One of the boxes I wasted more time was 'Laravel/Homestead' that failed since it is a virtual box developed in php envirnment and was not able to install dependencies. So I switched to 'bento/ubuntu-16.04' which uses Ubuntu 16.04 compatible with the dependencias I'm using. 

### 3.2 Change Vagrant file

Next I changed the vagrant file properties to use:

- config.vm.box = "bento/ubuntu-16.04"
- web.vm.provider "hyperv"

### 3.3 Execute vagrant 

After the changes on the vagrant file, I run git bash as administrator and run the following command:

	vagrant up --provider=hyperv

The build was successful:

![](./screenshots/7.png)

Then I ran the following command:

    vagrant status 

![](./screenshots/11.png)

And checked on Hyperv software aswell:

![](./screenshots/8.png)

### 3.3 Change IP address of the DB

Since none of the URLs were working I noticed I had to change the IP address of the DB in the application properties (inside vm web machine).

So I executed:

    vagrant ssh web
    cd devops-21-22-atb-1170053\CA3\Alternative\gradle_react-and-spring-data-rest-basic\src\main\resources
    nano application.properties

And changed: 

![](./screenshots/12.png)

To the DB IP machine address.

### 3.4 Check on web

Then I checked on the browser and everything was working:

![](./screenshots/9.png)

![](./screenshots/10.png)



