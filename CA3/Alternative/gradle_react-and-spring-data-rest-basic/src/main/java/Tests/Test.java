package Tests;

import com.greglturnquist.payroll.Employee;

import static org.testng.Assert.assertThrows;
import static org.testng.AssertJUnit.*;
import static org.testng.AssertJUnit.assertFalse;

public class Test {

    @org.testng.annotations.Test
    void stringNotNullOrEmptySusccess() {

        // Arrange
        String word = "Hello";

        // Act
        boolean result = Employee.stringNotNullOrEmpty(word);

        // Assert
        assertTrue(result);
    }
    @org.testng.annotations.Test
    void stringNotNullOrEmptyFaillingWhenNull() {

        // Arrange
        String word = null;

        // Act
        boolean result = Employee.stringNotNullOrEmpty(word);

        // Assert
        assertFalse(result);
    }

    @org.testng.annotations.Test
    void stringNotNullOrEmptyFaillingWhenEmpty() {

        // Arrange
        String word = "";

        // Act
        boolean result = Employee.stringNotNullOrEmpty(word);

        // Assert
        assertFalse(result);
    }

    @org.testng.annotations.Test
    void stringNotNullOrEmptyWhenInputIsEmail_Success() {

        // Arrange
        String word = "dinizpedro01@gmail.com";

        // Act
        boolean result = Employee.stringNotNullOrEmpty(word);

        // Assert
        assertTrue(result);
    }

    @org.testng.annotations.Test
    void higherOrEqualThanZeroSuccess() {

        // Arrange
        int number = 0;

        // Act
        boolean result = Employee.higherOrEqualThanZero(number);

        // Assert
        assertTrue(result);
    }

    @org.testng.annotations.Test
    void higherOrEqualThanZeroFaillingWhenBellowZero() {

        // Arrange
        int number = -5;

        // Act
        boolean result = Employee.higherOrEqualThanZero(number);

        // Assert
        assertFalse(result);
    }

    @org.testng.annotations.Test
    void CreatingEmployeeObject_Success() {

        // Arrange
        Employee employee = new Employee("Pedro", "Diniz", "Student", "Software Engineer", 5, "pedro@isep.ipp.pt");

        // Act

        // Assert
        assertEquals(employee, employee);
    }

    @org.testng.annotations.Test
    void creatingEmployeeObject_FaillingEmptyEmail() {

        // Arrange

        // Act

        // Assert
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Pedro", "Diniz", "Student", "Software Engineer", 5, "");
        });
    }

    @org.testng.annotations.Test
    void creatingEmployeeObject_FaillingEmailNull() {

        // Arrange

        // Act

        // Assert
        assertThrows(Exception.class, () -> {
            new Employee("Pedro", "Diniz", "Student", "Software Engineer", 5, null);
        });
    }

    @org.testng.annotations.Test
    void emailValidation_Success() {

        // Arrange
        String validEmail = "dinizpedro@gmail.com";

        // Act
        boolean result = Employee.emailValidation(validEmail);

        // Assert
        assertTrue(result);
    }

    @org.testng.annotations.Test
    void emailValidation_Failling() {

        // Arrange
        String validEmail = "dinizpedrogmail.com";

        // Act
        boolean result = Employee.emailValidation(validEmail);

        // Assert
        assertFalse(result);
    }
}
