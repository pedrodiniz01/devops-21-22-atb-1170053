# Class Assignment 3 Report

## 1. Analysis, Design and Implementation

### 1.1 First Week, virtual box - 09/05/2022

#### 1.1.1 Create virtual machine and clone repository:

I started by creating a virtual machine as explained in the lecture.

Then I cloned the repository to the remote pc:

       git clone https://pedrodiniz01@bitbucket.org/pedrodiniz01/devops-21-22-atb-1170053.git

![](./screenshots/1.png)

#### 1.1.2 Build and execute: CA1


To execute the CA1 project I started by changing directory:

        cd ./devops-21-22-atb-1170053/CA1/tut-react-and-spring-data-rest/basic

And executed the maven:

        ./mvnw spring-boot:run

I checked on the local host of the remote machine, the following link: http://192.168.56.5:8080/

   ![](./screenshots/2.png)

Since everything was working smoothly I started installing maven and gradle dependencies:

        sudo apt install maven
        sudo apt install gradle

#### 1.1.3 Build and execute: CA2 - Part One

Then I changed directory to the CA2 part one folder to test the gradle_basic_demo project.
I ran the following command and got an error:

        ./gradlew build

 ![](./screenshots/4.png)

So I changed the permissions using the following command:

        chmod u+x gradlew

Then I tried running 

        ./gradlew build

And the following error appeared: 

![](./screenshots/5.png)

After doing some research I found that the .jar file was missing. So I copied a new .jar file and rerun the command and it built succesfully.

![](./screenshots/6.png)

Then I run the "runServer" task and it run successfully:

![](./screenshots/7.png)

Next I tried running the "runClient" task and it failed:

![](./screenshots/8.png)

This error is happening because I'm trying to run code that is dependent on a display.

To solve this problem I've create the following task in gradle:

    task runClientOnVM(type:JavaExec, dependsOn: classes){
    group = "DevOps"
    description = "Launches a chat client that connects to a server on 192.168.56.5 "
  
    classpath = sourceSets.main.runtimeClasspath

    mainClass = 'basic_demo.ChatClientApp'

    args '192.168.56.5', '59001'
    }

Then I run this task on my pc and it worked:

![](./screenshots/9.png)

![](./screenshots/10.png)

#### 1.1.4 Build and execute: CA2 - Part Two

To end I tested the CA2 part two. I changed to the specific directory. And run the following command: 

        ./gradlew deleteGeneratedByyWebpack

It built succesfully:

![](./screenshots/12.png)

Then I checked if there were any files inside src/main/resources/static/built folder:

![](./screenshots/13.png)

Lastly I run the task to copy the generated jar to dist folder and it ran succesfully:

        ./gradlew backupJAR

![](./screenshots/11.png)

Then I checked if there was any jar located inside dist folder: 

![](./screenshots/14.png)