# Class Assignment 1 Report


## 1. Analysis, Design and Implementation

### 1.1 First Week, no branches - 14/03/2022

I started by creating a new bitbucket repository named as"devOps-21-22-ATB-1170052". Reading permission was given to Teacher Alexandre Bragança and it is accessible [here](https://bitbucket.org/pedrodiniz01/devops-21-22-atb-1170053/src/main/).

#### 1.1.1 Operative Guidelines:

To clone the React and Spring Data application I used the following command:

    git clone https://github.com/spring-guides/tut-react-and-spring-data-rest

Afterwards I pushed it to my own bitbucket repository using several commands.

First I added all the needed files to the staging area:

    git add .

Secondly I committed these files with the following message 'Initial Project'.

    git commit -m "Initial Project"

Lastly I pushed it to the remote bitbucket repository.

    git push

After this, I created a new folder named 'CA1' and copied the previous files and pasted them inside this folder. I pushed these changes using the following commands: 

    git add . 
    git commit -m "New folder with tutorial React and Spring Data Rest Application"
    git push

To create a git tag I used the following command:

    git tag v1.1.0

And to associate it with my last push:
    
    git push origin v1.1.0

Next, I started working on the various goals required for this assignment.

#### 1.1.2 Add a new field to record job years in the company

To start I created a bitbucket issue named "Add a new field with the years of the employee in the company".

To solve this issue, in the backend, I created the field "JobYears", adding it as an attribute in "Employee" class, creating a new getter, setter and toString function.

![](./screenshots/1.png)

In the frontend, I added a new data cell in "app.js" file.

![](./screenshots/2.png)

And added a new react elements in "bundle.js" file.

![](./screenshots/3.png)
![](./screenshots/4.png)

To verify that the changes were working I ran the application and opened the local host 8080:

![](./screenshots/11.png)

After confirming the implementation was working, I pushed this changes with the following commands: 

    git add . 
    git commit -m "Add new column with job years in company resolving issue #1"
    git push

Afterwards I created an issue to create methods that should validate data input.
These methods should validate that no null or empty values are accepted and that the jobYears column input only accepts values equal or higher than zero.

I created two methods 'stringNotNullOrEmpty' and 'higherOrEqualThanZero' and called them on constructor. 
When instantiating a new object if this two methods are not true an exception is thrown.

![](./screenshots/5.png)

I pushed this changes with the following commands: 

    git add . 
    git commit -m "Create methods to validate emplooye constructor inputs resolving issue #2"
    git push

Some unit tests were created to verify that these new methods are working correctly. I created an issue with this need and then added a few tests:

- When the string is neither null or empty and it should return true;

![](./screenshots/6.png)
<br>

- When the string is null and it should return false;

![](./screenshots/7.png)

- When the string in empty and it should return false;

![](./screenshots/8.png)

- When the jobYears input is 0 or higher and it should return true;

![](./screenshots/9.png)

- Lastly, when the jobYears input is below 0 and it should return false. 
    
![](./screenshots/10.png)

I pushed this changes with the following commands: 

    git add . 
    git commit -m "Tests method for validating input resolving issue #3"
    git push

I added two new tags and associated with the last push aswell:

    git tag v1.2.0
    git push origin v1.2.0
    git tag ca1-part1
    git push origin ca1-part1

#### 1.1.3 Backend debug

In order to debug the backend part of the application I've put a breakpoint at the constructor and run the debug option:
    
![](./screenshots/16.png)
![](./screenshots/13.png)

Since every parameter fullfilled the validations the object was instantiated successfully.

Then an object was instantiated with an invalid input: 

![](./screenshots/15.png)
![](./screenshots/14.png)

Since the job years field was negative (-33) the object was not instantiated and an exception was thrown.
The application wasn't executed as we can verify:

![](./screenshots/34.png)

#### 1.1.4 Frontend debug

On the frontend side I used the react developer tool extension to create breakpoints and debug the application:

![](./screenshots/17.png)

### 1.2 Second week Assignment, using branches - 21/03/2022

#### 1.2.1 Create branch and add new email field to the application 

For this second part of CA1 I started the assignment by creating a new branch named "email-field":

        git branch email-field


To create the email field. Few steps were made:

- As issue was created for this matter.
- On the backend side of the application, a new attribute was added 'email', getters, setters and toString function were created.

![](./screenshots/18.png)

- On the frontend side of the application, a new data cell was added in "app.js" file:

![](./screenshots/19.png)
![](./screenshots/20.png)

- And new react elements were added in "bundle.js" file:

![](./screenshots/21.png)
![](./screenshots/22.png)

To verify that the changes were working I ran the application and opened local host 8080:

![](./screenshots/29.png)

Then I commited and pushed the changes adressing the issue. The push was made to the email-field branch, using the following commands:

        git checkout email-field
        git add .
        git commit -m "Create email field resolving issue #4"
        git push origin email-field

Afterwards I created a single test with the method 'stringNotNullOrEmpty' that was previously created: 

![](./screenshots/23.png)

And changed the constructor to use this method to validate the email: 

![](./screenshots/24.png)

I committed and pushed the changes adressing the specific issue: 

        git add .
        git commit -m "Units test for email field resolving issue #5".
        git push origin email-field

Then I merged the email-field branch to the new branch, using the following commands:

        git checkout main
        git merge email-field
        git push 

And added a tag to my last push:

        git tag v1.3.0
        git push origin v1.3.0

#### 1.2.2 Create branch for fixing bugs (email must contain @)

To fix a bug that an email should always contain a "@", I created a new branch:

    git branch fix-invalid-email

A new method in java was created: 

![](./screenshots/25.png)

And it was included in the constructor:

![](./screenshots/26.png)


Changes were committed and pushed to fix-invalid-email branch:

    git check-out fix-invalid-email
    git add .
    git commit -m "Create method to accept valid emails containing @"
    git push origin fix-invalid-email

Afterwards two tests were created to validated this new method:

- Validate string when it contains "@" should return true:
![](./screenshots/27.png)

- Validate string when it does not contains "@" should return false:
![](./screenshots/28.png)

Then these changes were pushed to a branch named "fix-invalid-email":

        git add .
        git commit -m "Unit tests for validating email contains @"
        git push origin fix-invalid-email

Since the changes were tested I merged the secondary branch to the main branch:

    git checkout main
    git merge fix-invalid-email
    git push

Lastly I created two tags and associated them with my last push:

    git tag v1.3.1
    git push origin v1.3.1
    git tag ca1-part2
    git push origin ca1-part2

#### 1.2.3 Backend debug

In order to debug the backend part of the application I've put a breakpoint at the constructor and run the debug option:

![](./screenshots/30.png)
![](./screenshots/31.png)

Since every parameter fullfilled the validations the object was instantiated successfully.

<br>

Then an object was instantiated with an invalid input: 

![](./screenshots/32.png)
![](./screenshots/33.png)

Since the email field didn't contain an "@" the object was not instantiated and an exception was thrown.

The application wasn't executed as we can verify:

![](./screenshots/34.png)

#### 1.2.4 Frontend debug

On the frontend side I used the react developer tool extension to create breakpoints and debug the application:

![](./screenshots/35.png)


************************************************************************************************************
## 2. Analysis of an Alternative

As an alternative to Git I decided to analyze Mercurial. Both are distributed version control system used mostly by developers to bring a repo full of code down to their workstations, perform their work items, and then put it back into a central server. 
Overall, Git , which is owned my Microsoft, is way more popular, having a market share of 80 % in DVCS market, while Mercurial has an approx 2%. 

**GIT vs Mercurial (theoretical)**

These two tools are very similar, they help you manage the different versions of your projects, yet there are a few differences:

- When comparing both tools Git is more complex, it has a higher learning curve and more options to work with. Mercurial is easier to use but less flexible;
- Branching in Git is more effective, since they are references to a certain commit. This makes them lightweight yet powerful. Mercurial embeds the branches in the commits, where they are stored forever. This means that branches cannot be removed because that would alter the history. However, you can refer to certain commits with bookmarks, and use them in a similar manner to Git’s branches;
- Git supports a staging area, unlike Mercurial.


![](./screenshots/100.png)


************************************************************************************************************
## 3. Implementation of the Alternative
To implement the same features shown as before, dummie files were pushed to the remote repository. The purpose of this implementation is to show how Mercurial works using TortoiseHg which is a GUI.

First of all I committed and pushed the whole project:

![](./screenshots/38.png)

And checked if it was succesfully pushed to the remote repository:

![](./screenshots/39.png)

Then I created a new tag by right clicking the last commit, namming it "v1.1.0":

![](./screenshots/40.png)

Checked it on remote repository: 

![](./screenshots/41.png)

### 3.1 Add new field to record the years of the employee in the company

To test this feature I pushed a .txt file to resolve an issue that was created for this matter:

![](./screenshots/42.png)

![](./screenshots/43.png)

Afterwards I created two new tags named "v1.2.0" and "ca1-part1" and pushed them to the remote repository: 

![](./screenshots/44.png)

### 3.2 Create branch and add new email field to the application 

Then I created a new branch named "email-field":

![](./screenshots/45.png)

And pushed a .txt file to this newly created branch:

![](./screenshots/46.png)

![](./screenshots/47.png)

Then I merged the past changes to the default/main branch:

![](./screenshots/48.png)

And created a new tag named "v1.3.0":

![](./screenshots/49.png)

### 3.3 Create branch for fixing invalid email

Lastly I created a new branch named "fix-invalid-email":

![](./screenshots/50.png)

And commited and pushed a new .txt file name "mustContain@" :

![](./screenshots/51.png)

![](./screenshots/52.png)

And finally added two new tags "v1.3.1" and "ca1-part2":

![](./screenshots/53.png)

### 3.4 Conclusion

After implementing both tools and using them at a basic level, I feel that they are very similar:

- **TortoiseHG** was a good help to execute basic functions (pull, commit, push, clone, branches). Since it is a GUI it is simplier and more intuitive to use.
<br>

- **Support online** since GIT has a huge market share, finding the solutions to your problems online is much easier. I struggled a bit finding some information I needed about Mercurial. 


























    



