# Class Assignment 1 Report

Here you should write the report for the assignment.

You should use Markdown Syntax so that the report is properly rendered in Bitbucket. See how to write README files for Bitbucket in [here](https://confluence.atlassian.com/bitbucket/readme-content-221449772.html)

The source code for this assignment is located in the folder [ca1/tut-basic](https://bitbucket.org/atb/devops-19-20-rep-template/src/master/ca1/tut-basic)

As a suggestion, the report should have the following sections.

## 1. Analysis, Design and Implementation

### 1.1 First Class 11/03/2021

I started by creating a copy of the professor example repository in [here](https://confluence.atlassian.com/bitbucket/readme-content-221449772.html)

for duplicate the example repository I used the next command in gitBash

**git clone --bare https://bitbucket.org/atb/devops-21-rep-template.git**

the push it into my new repository

**git push -- mirror https://SwitchLR@bitbucket.org/SwitchLR/devops-20-21-1201769.git**

With my new repository ready I add the job title to the Employee as requested in the last page
of the first class presentation. Try it in the browser and everything went perfectly.

### 1.2 Second Class 18/03/2021

The five Requirements for CA1 were presented:

### 1.2.1
