# Class Assignment 2 Report


## 1. Analysis, Design and Implementation

### 1.1 First Week, simple gradle example - 31/03/2022

#### 1.1.1 Operative Guidelines:

To clone the Gradle Basic Demo I used the following command: 

    git clone https://pedrodiniz01@bitbucket.org/luisnogueira/gradle_basic_demo.git

Afterwards I created a new folder in my bitbucket repository named 'CA2' and pushed the application to this folder, using several commands:

First I added all the needed files to the staging area:

    git add .

Secondly I committed these files with the following message 'Initial Project'.

    git commit -m "Add Gradle Basic Demo resolving issue #6"

Lastly I pushed it to the remote bitbucket repository.

    git push

#### 1.1.2 Experimenting the chatroom application

To execute the application I executed the following commands from the readme file:

First I built a .jar file with the application:

    ./gradlew build 

It run succesfully:

![](./images/1.png)

Then to run the server:

    java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

![](./images/2.png)

Lastly I run the client:

    ./gradlew runClient

![](./images/3.png)

![](./images/4.png)


#### 1.1.2 Add a new task

To run the server I created the following task:

    task runServer(type:JavaExec, dependsOn: classes){

    group = "DevOps"

    description = "Launches a server"

    classpath = sourceSets.main.runtimeClasspath

    main = 'basic_demo.ChatServerApp'

    args '59001'}

And to run it on the terminal:

    gradle runServer

![](./images/5.png)


Afterwards I pushed this changes to the remote repository:

    git add .
    git commit -m "Add new task to run the server resolving issue #7"
    git push

#### 1.1.3 Add a simple unit test and update the gradle script 

Firstly I added the dependencies on the 'build.gradle' file:

    dependencies {

    // Use Apache Log4J for logging
    implementation group: 'org.apache.logging.log4j', name: 'log4j-api', version: '2.11.2'
    implementation group: 'org.apache.logging.log4j', name: 'log4j-core', version: '2.11.2'
    implementation 'junit:junit:4.13.1'
    testImplementation 'junit:junit:4.13.1

    }

Then I created a new java class for the test:

![](./images/7.png)

Afterwards I updated the 'build.gradle' so that it is able to execute the test. 

    test {
    useJUnit()
    }

Next I ran the following command and checked it it was successfully:

    gradle test

![](./images/6.png)

Lastly I pushed these changes:

    git add .
    git commit -m "Add simple test and update gradle script resolving issue #8"
    git push


#### 1.1.4 Add a new task of type Copy

I added the following code to the gradle script:

    task copyDocs(type: Copy) {
    from 'src/main/doc'
    into 'build/target/doc'
}

Then I run the script and verified the changes:

    gradle copyDocs

![](./images/8.png)


Lastly I pushed these changes to the remote repository:

    git add . 
    git commit -m "Add a new task of type Copy resolving issue #9"
    git push



#### 1.1.4 Add a new task of type Zip

I added the following code to the gradle script:

    task copyZip(type: Zip) {
    archiveFileName = "ZipBackup.zip"
    destinationDirectory = file("backup/zips")
    from "src"
    }

Then I ran the following command and verified the changes:

    gradle copyZip

![](./images/9.png)

Next I pushed the changes to the remote:

    git add . 
    git commit -m "Add a new task of type Zip resolving issue #10"
    git push

Lastly I created a new tag:

    git tag ca2-part1
    git push origin ca2-part1

